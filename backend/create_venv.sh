#!/usr/bin/env bash

rm -rf dist/ node_modules/
npm install
npx prisma generate
