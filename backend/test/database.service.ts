import { Injectable } from '@nestjs/common';
import { AcceptType, Algorithm, Job, Prisma } from '@prisma/client';
import { PrismaService } from '../src/database/prisma.service';

@Injectable()
export class DatabaseService {
  constructor(private prisma: PrismaService) {}

  async deleteAcceptType(
    where: Prisma.AcceptTypeWhereUniqueInput,
  ): Promise<AcceptType> {
    return this.prisma.acceptType.delete({ where });
  }

  async deleteAlgorithm(
    where: Prisma.AlgorithmWhereUniqueInput,
  ): Promise<Algorithm> {
    return this.prisma.algorithm.delete({ where });
  }

  async deleteJob(where: Prisma.JobWhereUniqueInput): Promise<Job> {
    await this.prisma.job.update({
      where,
      data: { algorithms: { deleteMany: {} }, results: { deleteMany: {} } },
    });
    return this.prisma.job.delete({ where });
  }

  async deleteMediaFiles() {
    return this.prisma.mediaFile.deleteMany({});
  }

  async deleteMessages() {
    return this.prisma.message.deleteMany({});
  }
}
