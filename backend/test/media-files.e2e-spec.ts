import { APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import {
  ClassSerializerInterceptor,
  INestApplication,
  ValidationPipe,
} from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { join } from 'path';
import { DatabaseModule } from '../src/database/database.module';
import { MediaFilesModule } from '../src/media-files/media-files.module';
import { DatabaseController } from '../test/database.controller';
import { DatabaseService } from '../test/database.service';
import * as request from 'supertest';

describe('MediaFiles', () => {
  let app: INestApplication;
  const data = { id: '', url: '' };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [DatabaseModule, MediaFilesModule],
      controllers: [DatabaseController],
      providers: [
        {
          provide: APP_PIPE,
          useValue: new ValidationPipe({
            transform: true,
            whitelist: true,
            skipMissingProperties: true,
          }),
        },
        { provide: APP_INTERCEPTOR, useClass: ClassSerializerInterceptor },
        DatabaseService,
      ],
    }).compile();
    app = moduleRef.createNestApplication();
    await app.init();
  });

  it('POST /media-files (media-file=undefined)', () => {
    return request(app.getHttpServer())
      .post('/media-files')
      .field('media_type', 'video')
      .expect(400);
  });

  it('POST /media-files (media-type=undefined)', () => {
    return request(app.getHttpServer())
      .post('/media-files')
      .attach('media_file', join(process.cwd(), 'test', 'video.mp4'))
      .expect(400);
  });

  it('POST /media-files (media-type=image)', () => {
    return request(app.getHttpServer())
      .post('/media-files')
      .field('media_type', 'image')
      .field('path', '/assets/image.png')
      .attach('media_file', join(process.cwd(), 'test', 'image.png'))
      .expect(201);
  });

  it('POST /media-files (media-type=audio)', () => {
    return request(app.getHttpServer())
      .post('/media-files')
      .field('media_type', 'audio')
      .field('path', '/assets/audio.png')
      .attach('media_file', join(process.cwd(), 'test', 'audio.wav'))
      .expect(201);
  });

  it('POST /media-files (media-type=video)', async () => {
    return request(app.getHttpServer())
      .post('/media-files')
      .field('media_type', 'video')
      .field('path', '/assets/video.mp4')
      .attach('media_file', join(process.cwd(), 'test', 'video.mp4'))
      .expect(201)
      .then((res) => {
        expect(res.body).toHaveProperty('id');
        expect(res.body).toHaveProperty('url');
        data.id = res.body.id;
        data.url = res.body.url;
      });
  });

  it('GET /media-files', async () => {
    return request(app.getHttpServer())
      .get('/media-files')
      .expect(200)
      .then((res) => {
        expect(res.body.length).toEqual(3);
      });
  });

  it('GET /media-files (media-type=image)', async () => {
    return request(app.getHttpServer())
      .get('/media-files?media-type=image')
      .expect(200)
      .then((res) => {
        expect(res.body.length).toEqual(1);
      });
  });

  it('GET /media-files (media-file=video.mp4)', async () => {
    return request(app.getHttpServer())
      .get(`/media-files/${data.id}`)
      .expect(200)
      .then((res) => {
        expect(res.body).toEqual(
          expect.objectContaining({
            media_type: 'video',
            path: '/assets/video.mp4',
            labels: [],
          }),
        );
      });
  });

  it('PUT /media-files (id=undefined)', () => {
    return request(app.getHttpServer())
      .put('/media-files')
      .send({ labels: ['video', 'images'] })
      .expect(400);
  });

  it('PUT /media-files (media-file=video.mp4)', async () => {
    return request(app.getHttpServer())
      .put('/media-files')
      .send({
        id: `${data.id}`,
        url: '',
        path: '',
        labels: ['video', 'images'],
      })
      .expect(200)
      .then((res) => {
        expect(res.body.url).not.toEqual('');
        expect(res.body).toEqual(
          expect.objectContaining({
            media_type: 'video',
            path: '/assets/video.mp4',
            labels: ['video', 'images'],
          }),
        );
      });
  });

  it('GET /media-files (label=images)', async () => {
    return request(app.getHttpServer())
      .get('/media-files?label=images')
      .expect(200)
      .then((res) => {
        expect(res.body.length).toEqual(1);
      });
  });

  it('GET /store (bucket=undefined)', () => {
    return request(app.getHttpServer()).get('/store').expect(404);
  });

  it('GET /store (object=undefined)', () => {
    return request(app.getHttpServer()).get('/store/media-files').expect(404);
  });

  it('GET /store (media-file=video.mp4)', () => {
    return request(app.getHttpServer()).get(data.url).expect(200);
  });

  it('DELETE /media-files', () => {
    return request(app.getHttpServer()).delete('/media-files').expect(200);
  });

  afterAll(async () => {
    await app.close();
  });
});
