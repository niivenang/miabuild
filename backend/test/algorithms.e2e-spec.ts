import { APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import {
  ClassSerializerInterceptor,
  INestApplication,
  ValidationPipe,
} from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { DatabaseModule } from '../src/database/database.module';
import { AlgorithmsModule } from '../src/algorithms/algorithms.module';
import { DatabaseController } from '../test/database.controller';
import { DatabaseService } from '../test/database.service';
import * as request from 'supertest';

describe('Algorithms', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [DatabaseModule, AlgorithmsModule],
      controllers: [DatabaseController],
      providers: [
        {
          provide: APP_PIPE,
          useValue: new ValidationPipe({
            transform: true,
            whitelist: true,
            skipMissingProperties: true,
          }),
        },
        { provide: APP_INTERCEPTOR, useClass: ClassSerializerInterceptor },
        DatabaseService,
      ],
    }).compile();
    app = moduleRef.createNestApplication();
    await app.init();
  });

  it('POST /algorithms (alias=undefined)', () => {
    return request(app.getHttpServer())
      .post('/algorithms')
      .send({ target: 'dev', name: 'sleep_5s@dev' })
      .expect(400);
  });

  it('POST /algorithms (target=undefined)', () => {
    return request(app.getHttpServer())
      .post('/algorithms')
      .send({ alias: 'sleep_5s', name: 'sleep_5s@dev' })
      .expect(400);
  });

  it('POST /algorithms (name=undefined)', () => {
    return request(app.getHttpServer())
      .post('/algorithms')
      .send({ alias: 'sleep_5s', target: 'dev' })
      .expect(400);
  });

  it('POST /algorithms (name=sleep_5s@dev)', async () => {
    return request(app.getHttpServer())
      .post('/algorithms')
      .send({
        alias: 'sleep_5s',
        target: 'dev',
        name: 'sleep_5s@dev',
        media_types: ['image'],
      })
      .expect(201)
      .then((res) => {
        expect(res.body).toEqual(
          expect.objectContaining({
            alias: 'sleep_5s',
            target: 'dev',
            name: 'sleep_5s@dev',
            media_types: ['image'],
          }),
        );
      });
  });

  it('POST /algorithms (name=sleep_5s@cpu)', async () => {
    return request(app.getHttpServer())
      .post('/algorithms')
      .send({
        alias: 'sleep_5s',
        target: 'cpu',
        name: 'sleep_5s@cpu',
        media_types: ['image'],
      })
      .expect(201)
      .then((res) => {
        expect(res.body).toEqual(
          expect.objectContaining({
            alias: 'sleep_5s',
            target: 'cpu',
            name: 'sleep_5s@cpu',
            media_types: ['image'],
          }),
        );
      });
  });

  it('POST /algorithms (name=sleep_10s@cpu)', async () => {
    return request(app.getHttpServer())
      .post('/algorithms')
      .send({
        alias: 'sleep_10s',
        target: 'cpu',
        name: 'sleep_10s@cpu',
        media_types: ['audio', 'video'],
      })
      .expect(201)
      .then((res) => {
        expect(res.body).toEqual(
          expect.objectContaining({
            alias: 'sleep_10s',
            target: 'cpu',
            name: 'sleep_10s@cpu',
            media_types: ['audio', 'video'],
          }),
        );
      });
  });

  it('GET /algorithms', async () => {
    return request(app.getHttpServer())
      .get('/algorithms')
      .expect(200)
      .then((res) => {
        expect(res.body.length).toEqual(3);
      });
  });

  it('GET /algorithms (target=cpu)', async () => {
    return request(app.getHttpServer())
      .get('/algorithms?target=cpu')
      .expect(200)
      .then((res) => {
        expect(res.body.length).toEqual(2);
      });
  });

  it('GET /algorithms (media-type=image)', async () => {
    return request(app.getHttpServer())
      .get('/algorithms?media-type=image')
      .expect(200)
      .then((res) => {
        expect(res.body.length).toEqual(2);
      });
  });

  it('GET /algorithms (target=cpu,media-type=image)', async () => {
    return request(app.getHttpServer())
      .get('/algorithms?target=cpu&media-type=image')
      .expect(200)
      .then((res) => {
        expect(res.body.length).toEqual(1);
      });
  });

  it('GET /algorithms (name=sleep_5s@dev)', async () => {
    return request(app.getHttpServer())
      .get('/algorithms/sleep_5s')
      .expect(200)
      .then((res) => {
        expect(res.body).toEqual(
          expect.objectContaining({
            alias: 'sleep_5s',
            target: 'dev',
            name: 'sleep_5s@dev',
            order: 1,
          }),
        );
      });
  });

  it('GET /algorithms (name=sleep_5s@cpu)', async () => {
    return request(app.getHttpServer())
      .get('/algorithms/sleep_5s?target=cpu')
      .expect(200)
      .then((res) => {
        expect(res.body).toEqual(
          expect.objectContaining({
            alias: 'sleep_5s',
            target: 'cpu',
            name: 'sleep_5s@cpu',
            order: 1,
          }),
        );
      });
  });

  it('PUT /algorithms (alias=undefined)', () => {
    return request(app.getHttpServer())
      .put('/algorithms')
      .send({ target: 'cpu' })
      .expect(400);
  });

  it('PUT /algorithms (target=undefined)', () => {
    return request(app.getHttpServer())
      .put('/algorithms')
      .send({ alias: 'splice' })
      .expect(400);
  });

  it('PUT /algorithms (name=sleep_5s@cpu)', async () => {
    return request(app.getHttpServer())
      .put('/algorithms')
      .send({
        alias: 'sleep_5s',
        target: 'cpu',
        order: 99,
        media_types: ['video'],
      })
      .expect(200)
      .then((res) => {
        expect(res.body).toEqual(
          expect.objectContaining({
            alias: 'sleep_5s',
            target: 'cpu',
            name: 'sleep_5s@cpu',
            order: 99,
            media_types: ['video'],
          }),
        );
      });
  });

  it('DELETE /algorithms (name=sleep_5s@dev)', () => {
    return request(app.getHttpServer())
      .delete('/algorithms/sleep_5s')
      .expect(200);
  });

  it('DELETE /algorithms (name=sleep_5s@cpu)', () => {
    return request(app.getHttpServer())
      .delete('/algorithms/sleep_5s?target=cpu')
      .expect(200);
  });

  it('DELETE /algorithms (name=sleep_10s@cpu)', () => {
    return request(app.getHttpServer())
      .delete('/algorithms/sleep_10s?target=cpu')
      .expect(200);
  });

  afterAll(async () => {
    await app.close();
  });
});
