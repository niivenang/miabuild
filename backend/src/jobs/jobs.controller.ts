import {
  BadRequestException,
  Body,
  ConflictException,
  Controller,
  Get,
  NotFoundException,
  Param,
  ParseUUIDPipe,
  Post,
  Put,
  Query,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiTags } from '@nestjs/swagger';
import {
  Algorithm,
  JobStatus,
  MediaType,
  Prisma,
  Target,
} from '@prisma/client';
import { instanceToPlain } from 'class-transformer';
import { isInteractive } from '../helpers/job-mode.pipe';
import { SettingsService } from '../database/settings.service';
import { MinioClientService } from '../datastore/minio-client.service';
import { AlgorithmsService } from '../algorithms/algorithms.service';
import { MediaFilesService } from '../media-files/media-files.service';
import { MessagesService } from '../messages/messages.service';
import { JobsService } from './jobs.service';
import {
  CreateJobDto,
  CreateJobWithMediaFileFormDto,
  GetJobsQueryDto,
  JobDto,
  ListJobsQueryDto,
  UpdateJobDto,
} from './jobs.transfer';

@ApiTags('jobs')
@Controller('jobs')
export class JobsController {
  constructor(
    private readonly jobsService: JobsService,
    private readonly settingsService: SettingsService,
    private readonly algorithmsService: AlgorithmsService,
    private readonly mediaFilesService: MediaFilesService,
    private readonly messagesService: MessagesService,
    private eventEmitter: EventEmitter2,
    private minioClient: MinioClientService,
  ) {}

  @Get(':id')
  async getJob(@Param('id', ParseUUIDPipe) id: string): Promise<JobDto> {
    const job = await this.jobsService.job({ id });
    if (job === null) {
      throw new NotFoundException();
    }
    return new JobDto(job);
  }

  @Get('media-files/:fid')
  async getJobs(
    @Param('fid', ParseUUIDPipe) fid: string,
    @Query() params: GetJobsQueryDto,
  ): Promise<JobDto[]> {
    const { mode, status, startDate, endDate, offset, limit } = params;
    const where: Prisma.JobWhereInput = {
      fid,
      status,
      AND: [{ created: { gte: startDate } }, { created: { lte: endDate } }],
    };
    if (mode !== undefined) {
      where.interactive = isInteractive(mode);
    }
    const jobs = await this.jobsService.jobs({
      where,
      skip: offset,
      take: limit,
      orderBy: { modified: 'desc' },
    });
    return jobs.map((job) => new JobDto(job));
  }

  @Get()
  async listJobs(@Query() params: ListJobsQueryDto): Promise<JobDto[]> {
    const { label, mode, status, startDate, endDate, offset, limit } = params;
    const where: Prisma.JobWhereInput = {
      status,
      AND: [{ created: { gte: startDate } }, { created: { lte: endDate } }],
    };
    if (label !== undefined) {
      where.labels = { has: label };
    }
    if (mode !== undefined) {
      where.interactive = isInteractive(mode);
    }
    const jobs = await this.jobsService.jobs({
      where,
      skip: offset,
      take: limit,
      orderBy: { modified: 'desc' },
    });
    return jobs.map((job) => new JobDto(job));
  }

  async getAlgorithms(
    interactive: boolean,
    data: CreateJobWithMediaFileFormDto,
  ): Promise<Prisma.JobsAlgorithmsCreateWithoutJobInput[]> {
    let picks: Algorithm[] = [];
    const settings = await this.settingsService.settings();
    if (settings.useBatch && !interactive) {
      picks = await this.algorithmsService.algorithms({
        where: {
          target: Target.BATCH,
          types: { has: data.type },
          enabled: true,
        },
      });
    } else {
      picks = await this.algorithmsService.algorithms({
        where: {
          target: { not: Target.BATCH },
          types: { has: data.type },
          enabled: true,
        },
      });
    }
    if (picks.length === 0) {
      throw new BadRequestException();
    }

    const algorithms: Prisma.JobsAlgorithmsCreateWithoutJobInput[] = [];
    for (const algorithm of picks) {
      algorithms.push({
        algorithm: {
          connect: {
            alias_target: { alias: algorithm.alias, target: algorithm.target },
          },
        },
      });
    }
    return algorithms;
  }

  @Post('media-files')
  @UseInterceptors(FileInterceptor('media_file'))
  async createJobWithMediaFile(
    @Body() data: CreateJobWithMediaFileFormDto,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<JobDto> {
    if (file === undefined) {
      throw new BadRequestException();
    }

    try {
      const interactive = isInteractive(data.mode);
      const algorithms = await this.getAlgorithms(interactive, data);
      const url = await this.minioClient.upload(file, 'media-files');
      const { type, path } = data;
      const job = await this.jobsService.createJob({
        file: { create: { type, url, path } },
        algorithms: { create: algorithms },
        interactive,
      });
      if (job === null) {
        throw new ConflictException();
      }

      await this.messagesService.createMessage({
        source: 'jobs.controller.create',
        content: JSON.stringify(instanceToPlain(new JobDto(job))),
      });
      this.eventEmitter.emit('job.created', job);
      return new JobDto(job);
    } catch (ex) {
      if (ex instanceof BadRequestException) {
        throw ex;
      } else if (ex instanceof ConflictException) {
        throw ex;
      } else {
        throw new BadRequestException();
      }
    }
  }

  async getOrPickAlgorithms(
    type: MediaType,
    data: CreateJobDto,
  ): Promise<Prisma.JobsAlgorithmsCreateWithoutJobInput[]> {
    let picks: Algorithm[] = [];
    if (data.algorithms === undefined || data.algorithms.length === 0) {
      const settings = await this.settingsService.settings();
      if (
        settings.useBatch &&
        data.interactive !== undefined &&
        !data.interactive
      ) {
        picks = await this.algorithmsService.algorithms({
          where: {
            target: Target.BATCH,
            types: { has: type },
            enabled: true,
          },
        });
      } else {
        picks = await this.algorithmsService.algorithms({
          where: {
            target: { not: Target.BATCH },
            types: { has: type },
            enabled: true,
          },
        });
      }
    } else {
      for (const pick of data.algorithms) {
        const algorithm = await this.algorithmsService.algorithm({
          alias_target: { alias: pick.alias, target: pick.target },
        });
        if (algorithm !== null) {
          picks.push(algorithm);
        }
      }
    }
    if (picks.length === 0) {
      throw new BadRequestException();
    }

    const algorithms: Prisma.JobsAlgorithmsCreateWithoutJobInput[] = [];
    for (const algorithm of picks) {
      algorithms.push({
        algorithm: {
          connect: {
            alias_target: { alias: algorithm.alias, target: algorithm.target },
          },
        },
      });
    }
    return algorithms;
  }

  @Post()
  async createJob(@Body() data: CreateJobDto): Promise<JobDto> {
    const mediaFile = await this.mediaFilesService.mediaFile({ id: data.fid });
    if (mediaFile === null) {
      throw new BadRequestException();
    }

    const algorithms = await this.getOrPickAlgorithms(mediaFile.type, data);
    const { fid, parameters, interactive, labels } = data;
    const job = await this.jobsService.createJob({
      file: { connect: { id: fid } },
      algorithms: { create: algorithms },
      parameters,
      interactive,
      labels,
    });
    if (job === null) {
      throw new ConflictException();
    }

    await this.messagesService.createMessage({
      source: 'jobs.controller.create',
      content: JSON.stringify(instanceToPlain(new JobDto(job))),
    });
    this.eventEmitter.emit('job.created', job);
    return new JobDto(job);
  }

  @Put()
  async updateJob(@Body() data: UpdateJobDto): Promise<JobDto> {
    if (data.status !== undefined) {
      const job = await this.jobsService.job({ id: data.id });
      if (job === null) {
        throw new NotFoundException();
      }
      if (
        data.status === JobStatus.WAITING ||
        data.status === JobStatus.PENDING
      ) {
        data.status = job.status;
      }
    }
    const job = await this.jobsService.updateJob({
      where: { id: data.id },
      data,
    });
    if (job === null) {
      throw new ConflictException();
    }

    await this.messagesService.createMessage({
      source: 'jobs.controller.update',
      content: JSON.stringify(instanceToPlain(new JobDto(job))),
    });
    if (
      job.status === JobStatus.COMPLETED ||
      job.status === JobStatus.TERMINATED
    ) {
      this.eventEmitter.emit('job.finalized', job);
    }
    return new JobDto(job);
  }
}
