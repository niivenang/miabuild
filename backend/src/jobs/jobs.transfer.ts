import { Job, JobStatus, MediaType, Target } from '@prisma/client';
import { Expose, Transform, Type } from 'class-transformer';
import {
  IsArray,
  IsBoolean,
  IsDate,
  IsDefined,
  IsIn,
  IsJSON,
  IsNotEmpty,
  IsNumber,
  IsString,
  IsUUID,
  ValidateNested,
} from 'class-validator';
import { AlgorithmDto } from '../algorithms/algorithms.transfer';
import { MediaFileDto } from '../media-files/media-files.transfer';
import { ResultDto } from '../results/results.transfer';
import * as JobModeTransform from '../helpers/job-mode.pipe';
import * as JobStatusTransform from '../helpers/job-status.pipe';
import * as MediaTypeTransform from '../helpers/media-type.pipe';
import * as TargetTransform from '../helpers/target.pipe';

export class GetJobsQueryDto {
  @Transform(JobModeTransform.toClass, { toClassOnly: true })
  @Transform(JobModeTransform.toPlain, { toPlainOnly: true })
  @IsIn(JobModeTransform.getOptions())
  mode: string;

  @Transform(JobStatusTransform.toClass, { toClassOnly: true })
  @Transform(JobStatusTransform.toPlain, { toPlainOnly: true })
  @IsIn(JobStatusTransform.getOptions())
  status: JobStatus;

  @Expose({ name: 'start-date' })
  @Type(() => Date)
  @IsDate()
  startDate: Date;

  @Expose({ name: 'end-date' })
  @Type(() => Date)
  @IsDate()
  endDate: Date;

  @Type(() => Number)
  @IsNumber()
  offset = 0;

  @Type(() => Number)
  @IsNumber()
  limit = 100;
}

export class ListJobsQueryDto {
  @IsString()
  label: string;

  @Transform(JobModeTransform.toClass, { toClassOnly: true })
  @Transform(JobModeTransform.toPlain, { toPlainOnly: true })
  @IsIn(JobModeTransform.getOptions())
  mode: string;

  @Transform(JobStatusTransform.toClass, { toClassOnly: true })
  @Transform(JobStatusTransform.toPlain, { toPlainOnly: true })
  @IsIn(JobStatusTransform.getOptions())
  status: JobStatus;

  @Expose({ name: 'start-date' })
  @Type(() => Date)
  @IsDate()
  startDate: Date;

  @Expose({ name: 'end-date' })
  @Type(() => Date)
  @IsDate()
  endDate: Date;

  @Type(() => Number)
  @IsNumber()
  offset = 0;

  @Type(() => Number)
  @IsNumber()
  limit = 100;
}

export class PickAlgorithmDto {
  @IsNotEmpty()
  @IsString()
  alias: string;

  @Transform(TargetTransform.toClass, { toClassOnly: true })
  @Transform(TargetTransform.toPlain, { toPlainOnly: true })
  @IsIn(TargetTransform.getOptions())
  target: Target;
}

export class CreateJobDto {
  @Expose({ name: 'file_id' })
  @IsDefined()
  @IsUUID()
  fid: string;

  @IsJSON()
  parameters: string;

  @IsBoolean()
  interactive: boolean;

  @IsArray()
  @IsString({ each: true })
  labels: string[];

  @ValidateNested()
  @Type(() => PickAlgorithmDto)
  algorithms: PickAlgorithmDto[];
}

export class CreateJobWithMediaFileFormDto {
  @Expose({ name: 'media_type' })
  @Transform(MediaTypeTransform.toClass, { toClassOnly: true })
  @Transform(MediaTypeTransform.toPlain, { toPlainOnly: true })
  @IsDefined()
  @IsIn(MediaTypeTransform.getOptions())
  type: MediaType;

  @IsString()
  path: string;

  @Transform(JobModeTransform.toClass, { toClassOnly: true })
  @Transform(JobModeTransform.toPlain, { toPlainOnly: true })
  @IsIn(JobModeTransform.getOptions())
  mode: string;
}

export class UpdateJobDto {
  @IsDefined()
  @IsUUID()
  id: string;

  @Transform(JobStatusTransform.toClass, { toClassOnly: true })
  @Transform(JobStatusTransform.toPlain, { toPlainOnly: true })
  @IsIn(JobStatusTransform.getOptions())
  status: JobStatus;

  @IsArray()
  @IsString({ each: true })
  labels: string[];
}

export class JobAlgorithmDto {
  @Expose({ name: 'job_id' })
  jid: string;

  alias: string;

  @Transform(TargetTransform.toClass, { toClassOnly: true })
  @Transform(TargetTransform.toPlain, { toPlainOnly: true })
  target: string;

  @Expose({ toPlainOnly: true })
  created: Date;

  @Type(() => AlgorithmDto)
  algorithm: AlgorithmDto;
}

export class JobDto {
  id: string;

  @Expose({ name: 'file_id' })
  fid: string;

  parameters: string;

  interactive: boolean;

  @Transform(JobStatusTransform.toClass, { toClassOnly: true })
  @Transform(JobStatusTransform.toPlain, { toPlainOnly: true })
  status: JobStatus;

  labels: string[];

  @Expose({ toPlainOnly: true })
  created: Date;

  @Expose({ toPlainOnly: true })
  modified: Date;

  @Type(() => MediaFileDto)
  file: MediaFileDto;

  @Type(() => JobAlgorithmDto)
  algorithms: JobAlgorithmDto[];

  @Type(() => ResultDto)
  results: ResultDto[];

  constructor(partial: Partial<Job>) {
    Object.assign(this, partial);
  }
}
