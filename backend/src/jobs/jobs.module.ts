import { Module } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { DatastoreModule } from '../datastore/datastore.module';
import { AlgorithmsModule } from '../algorithms/algorithms.module';
import { MediaFilesModule } from '../media-files/media-files.module';
import { MessagesModule } from '../messages/messages.module';
import { JobsController } from './jobs.controller';
import { JobsService } from './jobs.service';

@Module({
  imports: [
    DatabaseModule,
    AlgorithmsModule,
    MediaFilesModule,
    MessagesModule,
    DatastoreModule,
  ],
  controllers: [JobsController],
  providers: [JobsService],
  exports: [JobsService],
})
export class JobsModule {}
