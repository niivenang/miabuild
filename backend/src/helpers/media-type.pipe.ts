import { MediaType } from '@prisma/client';

export function getOptions() {
  return Object.values(MediaType);
}

export function toClass({ value }) {
  return value?.toUpperCase();
}

export function toClassArray({ value }) {
  return value?.map((mediaType: string) => mediaType.toUpperCase());
}

export function toPlain({ value }) {
  return value?.toLowerCase();
}

export function toPlainArray({ value }) {
  return value?.map((mediaType: string) => mediaType.toLowerCase());
}
