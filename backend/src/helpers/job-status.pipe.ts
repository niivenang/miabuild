import { JobStatus } from '@prisma/client';

export function getOptions() {
  return Object.values(JobStatus);
}

export function toClass({ value }) {
  return value?.toUpperCase();
}

export function toPlain({ value }) {
  return value?.toLowerCase();
}
