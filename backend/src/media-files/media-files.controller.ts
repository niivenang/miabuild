import {
  BadRequestException,
  Body,
  ConflictException,
  Controller,
  Get,
  NotFoundException,
  Param,
  ParseUUIDPipe,
  Post,
  Put,
  Query,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiTags } from '@nestjs/swagger';
import { Prisma } from '@prisma/client';
import { instanceToPlain } from 'class-transformer';
import { MinioClientService } from '../datastore/minio-client.service';
import { MessagesService } from '../messages/messages.service';
import { MediaFilesService } from './media-files.service';
import {
  CreateMediaFileFormDto,
  ListMediaFilesQueryDto,
  MediaFileDto,
  UpdateMediaFileDto,
} from './media-files.transfer';

@ApiTags('media-files')
@Controller('media-files')
export class MediaFilesController {
  constructor(
    private readonly mediaFilesService: MediaFilesService,
    private readonly messagesService: MessagesService,
    private minioClient: MinioClientService,
  ) {}

  @Get(':id')
  async getMediaFile(
    @Param('id', ParseUUIDPipe) id: string,
  ): Promise<MediaFileDto> {
    const mediaFile = await this.mediaFilesService.mediaFile({ id });
    if (mediaFile === null) {
      throw new NotFoundException();
    }
    return new MediaFileDto(mediaFile);
  }

  @Get()
  async listMediaFiles(
    @Query() params: ListMediaFilesQueryDto,
  ): Promise<MediaFileDto[]> {
    const { path, label, mediaType, startDate, endDate, offset, limit } =
      params;
    const where: Prisma.MediaFileWhereInput = {
      type: mediaType,
      AND: [{ created: { gte: startDate } }, { created: { lte: endDate } }],
    };
    if (path !== undefined) {
      where.path = { contains: path, mode: 'insensitive' };
    }
    if (label !== undefined) {
      where.labels = { has: label };
    }
    const mediaFiles = await this.mediaFilesService.mediaFiles({
      where,
      skip: offset,
      take: limit,
      orderBy: { modified: 'desc' },
    });
    return mediaFiles.map((mediaFile) => new MediaFileDto(mediaFile));
  }

  @Post()
  @UseInterceptors(FileInterceptor('media_file'))
  async createMediaFile(
    @Body() data: CreateMediaFileFormDto,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<MediaFileDto> {
    if (file === undefined) {
      throw new BadRequestException();
    }

    try {
      const url = await this.minioClient.upload(file, 'media-files');
      const { type, path } = data;
      const mediaFile = await this.mediaFilesService.createMediaFile({
        type,
        url,
        path,
      });
      if (mediaFile === null) {
        throw new ConflictException();
      }

      await this.messagesService.createMessage({
        source: 'media-files.controller.create',
        content: JSON.stringify(instanceToPlain(new MediaFileDto(mediaFile))),
      });
      return new MediaFileDto(mediaFile);
    } catch (ex) {
      if (ex instanceof ConflictException) {
        throw ex;
      } else {
        throw new BadRequestException();
      }
    }
  }

  @Put()
  async updateMediaFile(
    @Body() data: UpdateMediaFileDto,
  ): Promise<MediaFileDto> {
    const mediaFile = await this.mediaFilesService.updateMediaFile({
      where: { id: data.id },
      data,
    });
    if (mediaFile === null) {
      throw new ConflictException();
    }

    await this.messagesService.createMessage({
      source: 'media-files.controller.update',
      content: JSON.stringify(instanceToPlain(new MediaFileDto(mediaFile))),
    });
    return new MediaFileDto(mediaFile);
  }
}
