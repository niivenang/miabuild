import {
  Body,
  ConflictException,
  Controller,
  Get,
  NotFoundException,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Prisma } from '@prisma/client';
import { instanceToPlain } from 'class-transformer';
import { MessagesService } from '../messages/messages.service';
import { AlgorithmsService } from './algorithms.service';
import {
  AlgorithmDto,
  CreateAlgorithmDto,
  GetAlgorithmQueryDto,
  ListAlgorithmsQueryDto,
  UpdateAlgorithmDto,
} from './algorithms.transfer';

@ApiTags('algorithms')
@Controller('algorithms')
export class AlgorithmsController {
  constructor(
    private readonly algorithmsService: AlgorithmsService,
    private readonly messagesService: MessagesService,
  ) {}

  @Get(':alias')
  async getAlgorithm(
    @Param('alias') alias: string,
    @Query() params: GetAlgorithmQueryDto,
  ): Promise<AlgorithmDto> {
    const { target } = params;
    const algorithm = await this.algorithmsService.algorithm({
      alias_target: { alias: alias, target: target },
    });
    if (algorithm === null) {
      throw new NotFoundException();
    }
    return new AlgorithmDto(algorithm);
  }

  @Get()
  async listAlgorithms(
    @Query() params: ListAlgorithmsQueryDto,
  ): Promise<AlgorithmDto[]> {
    const { target, mediaType, offset, limit } = params;
    const where: Prisma.AlgorithmWhereInput = { target };
    if (mediaType !== undefined) {
      where.types = { has: mediaType };
    }
    const algorithms = await this.algorithmsService.algorithms({
      where,
      skip: offset,
      take: limit,
      orderBy: { modified: 'desc' },
    });
    return algorithms.map((algorithm) => new AlgorithmDto(algorithm));
  }

  @Post()
  async createAlgorithm(
    @Body() data: CreateAlgorithmDto,
  ): Promise<AlgorithmDto> {
    const algorithm = await this.algorithmsService.createAlgorithm(data);
    if (algorithm === null) {
      throw new ConflictException();
    }

    await this.messagesService.createMessage({
      source: 'algorithms.controller.create',
      content: JSON.stringify(instanceToPlain(new AlgorithmDto(algorithm))),
    });
    return new AlgorithmDto(algorithm);
  }

  @Put()
  async updateAlgorithm(
    @Body() data: UpdateAlgorithmDto,
  ): Promise<AlgorithmDto> {
    const algorithm = await this.algorithmsService.updateAlgorithm({
      where: { alias_target: { alias: data.alias, target: data.target } },
      data,
    });
    if (algorithm === null) {
      throw new ConflictException();
    }

    await this.messagesService.createMessage({
      source: 'algorithms.controller.update',
      content: JSON.stringify(instanceToPlain(new AlgorithmDto(algorithm))),
    });
    return new AlgorithmDto(algorithm);
  }
}
