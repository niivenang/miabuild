import { Algorithm, MediaType, Target } from '@prisma/client';
import { Exclude, Expose, Transform, Type } from 'class-transformer';
import {
  IsArray,
  IsBoolean,
  IsDefined,
  IsIn,
  IsNotEmpty,
  IsNumber,
  IsString,
  IsUrl,
} from 'class-validator';
import * as MediaTypeTransform from '../helpers/media-type.pipe';
import * as TargetTransform from '../helpers/target.pipe';

export class GetAlgorithmQueryDto {
  @Transform(TargetTransform.toClass, { toClassOnly: true })
  @Transform(TargetTransform.toPlain, { toPlainOnly: true })
  @IsIn(TargetTransform.getOptions())
  target: Target = Target.DEVELOPMENT;
}

export class ListAlgorithmsQueryDto {
  @Transform(TargetTransform.toClass, { toClassOnly: true })
  @Transform(TargetTransform.toPlain, { toPlainOnly: true })
  @IsIn(TargetTransform.getOptions())
  target: Target;

  @Expose({ name: 'media-type' })
  @Transform(MediaTypeTransform.toClass, { toClassOnly: true })
  @Transform(MediaTypeTransform.toPlain, { toPlainOnly: true })
  @IsIn(MediaTypeTransform.getOptions())
  mediaType: MediaType;

  @Type(() => Number)
  @IsNumber()
  offset = 0;

  @Type(() => Number)
  @IsNumber()
  limit = 100;
}

export class CreateAlgorithmDto {
  @IsDefined()
  @IsNotEmpty()
  @IsString()
  alias: string;

  @Transform(TargetTransform.toClass, { toClassOnly: true })
  @Transform(TargetTransform.toPlain, { toPlainOnly: true })
  @IsDefined()
  @IsIn(TargetTransform.getOptions())
  target: Target;

  @IsDefined()
  @IsString()
  name: string;

  @IsNumber()
  order: number;

  @IsString()
  description: string;

  @Expose({ name: 'badge_description' })
  @IsString()
  badgeDesc: string;

  @Expose({ name: 'media_types' })
  @Transform(MediaTypeTransform.toClassArray, { toClassOnly: true })
  @Transform(MediaTypeTransform.toPlainArray, { toPlainOnly: true })
  @IsArray()
  @IsIn(MediaTypeTransform.getOptions(), { each: true })
  types: MediaType[];

  @IsUrl()
  url: string;

  @IsBoolean()
  enabled: boolean;
}

export class UpdateAlgorithmDto {
  @IsDefined()
  @IsNotEmpty()
  @IsString()
  alias: string;

  @Transform(TargetTransform.toClass, { toClassOnly: true })
  @Transform(TargetTransform.toPlain, { toPlainOnly: true })
  @IsDefined()
  @IsIn(TargetTransform.getOptions())
  target: Target;

  @IsString()
  name: string;

  @IsNumber()
  order: number;

  @IsString()
  description: string;

  @Expose({ name: 'badge_description' })
  @IsString()
  badgeDesc: string;

  @Expose({ name: 'media_types' })
  @Transform(MediaTypeTransform.toClassArray, { toClassOnly: true })
  @Transform(MediaTypeTransform.toPlainArray, { toPlainOnly: true })
  @IsArray()
  @IsIn(MediaTypeTransform.getOptions(), { each: true })
  types: MediaType[];

  @IsUrl()
  url: string;

  @IsBoolean()
  enabled: boolean;
}

export class AlgorithmDto {
  alias: string;

  @Transform(TargetTransform.toClass, { toClassOnly: true })
  @Transform(TargetTransform.toPlain, { toPlainOnly: true })
  target: Target;

  name: string;

  order: number;

  description: string;

  @Expose({ name: 'badge_description' })
  badgeDesc: string;

  @Expose({ name: 'media_types' })
  @Transform(MediaTypeTransform.toClassArray, { toClassOnly: true })
  @Transform(MediaTypeTransform.toPlainArray, { toPlainOnly: true })
  types: MediaType[];

  @Exclude()
  inputs: string;

  url: string;

  enabled: boolean;

  @Expose({ toPlainOnly: true })
  created: Date;

  @Expose({ toPlainOnly: true })
  modified: Date;

  constructor(partial: Partial<Algorithm>) {
    Object.assign(this, partial);
  }
}
