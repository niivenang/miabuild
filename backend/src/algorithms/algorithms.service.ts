import { Injectable } from '@nestjs/common';
import { Algorithm, Prisma } from '@prisma/client';
import { PrismaService } from '../database/prisma.service';

@Injectable()
export class AlgorithmsService {
  constructor(private prisma: PrismaService) {}

  async algorithm(where: Prisma.AlgorithmWhereUniqueInput): Promise<Algorithm> {
    return this.prisma.algorithm.findUnique({ where });
  }

  async algorithms(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.AlgorithmWhereUniqueInput;
    where?: Prisma.AlgorithmWhereInput;
    orderBy?: Prisma.AlgorithmOrderByWithRelationInput;
  }): Promise<Algorithm[]> {
    const { skip, take, cursor, where, orderBy } = params;
    return this.prisma.algorithm.findMany({
      skip,
      take,
      cursor,
      where,
      orderBy,
    });
  }

  async createAlgorithm(data: Prisma.AlgorithmCreateInput): Promise<Algorithm> {
    return this.prisma.algorithm.create({ data });
  }

  async updateAlgorithm(params: {
    where: Prisma.AlgorithmWhereUniqueInput;
    data: Prisma.AlgorithmUpdateInput;
  }): Promise<Algorithm> {
    const { where, data } = params;
    return this.prisma.algorithm.update({ data, where });
  }
}
