import {
  Body,
  ConflictException,
  Controller,
  Get,
  NotFoundException,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { instanceToPlain } from 'class-transformer';
import { MessagesService } from '../messages/messages.service';
import { AcceptTypesService } from './accept-types.service';
import {
  AcceptTypeDto,
  CreateAcceptTypeDto,
  ListAcceptTypesQueryDto,
  UpdateAcceptTypeDto,
} from './accept-types.transfer';

@ApiTags('accept-types')
@Controller('accept-types')
export class AcceptTypesController {
  constructor(
    private readonly acceptTypesService: AcceptTypesService,
    private readonly messagesService: MessagesService,
  ) {}

  @Get(':suffix')
  async getAcceptType(@Param('suffix') suffix: string): Promise<AcceptTypeDto> {
    const acceptType = await this.acceptTypesService.acceptType({ suffix });
    if (acceptType === null) {
      throw new NotFoundException();
    }
    return new AcceptTypeDto(acceptType);
  }

  @Get()
  async listAcceptTypes(
    @Query() params: ListAcceptTypesQueryDto,
  ): Promise<AcceptTypeDto[]> {
    const { mediaType, offset, limit } = params;
    const acceptTypes = await this.acceptTypesService.acceptTypes({
      where: { type: mediaType },
      skip: offset,
      take: limit,
      orderBy: { modified: 'desc' },
    });
    return acceptTypes.map((acceptType) => new AcceptTypeDto(acceptType));
  }

  @Post()
  async createAcceptType(
    @Body() data: CreateAcceptTypeDto,
  ): Promise<AcceptTypeDto> {
    const acceptType = await this.acceptTypesService.createAcceptType(data);
    if (acceptType === null) {
      throw new ConflictException();
    }

    await this.messagesService.createMessage({
      source: 'accept-types.controller.create',
      content: JSON.stringify(instanceToPlain(new AcceptTypeDto(acceptType))),
    });
    return new AcceptTypeDto(acceptType);
  }

  @Put()
  async updateAcceptType(
    @Body() data: UpdateAcceptTypeDto,
  ): Promise<AcceptTypeDto> {
    const acceptType = await this.acceptTypesService.updateAcceptType({
      where: { suffix: data.suffix },
      data,
    });
    if (acceptType === null) {
      throw new ConflictException();
    }

    await this.messagesService.createMessage({
      source: 'accept-types.controller.update',
      content: JSON.stringify(instanceToPlain(new AcceptTypeDto(acceptType))),
    });
    return new AcceptTypeDto(acceptType);
  }
}
