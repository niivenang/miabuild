import { Injectable } from '@nestjs/common';
import { AcceptType, Prisma } from '@prisma/client';
import { PrismaService } from '../database/prisma.service';

@Injectable()
export class AcceptTypesService {
  constructor(private prisma: PrismaService) {}

  async acceptType(
    where: Prisma.AcceptTypeWhereUniqueInput,
  ): Promise<AcceptType> {
    return this.prisma.acceptType.findUnique({ where });
  }

  async acceptTypes(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.AcceptTypeWhereUniqueInput;
    where?: Prisma.AcceptTypeWhereInput;
    orderBy?: Prisma.AcceptTypeOrderByWithRelationInput;
  }): Promise<AcceptType[]> {
    const { skip, take, cursor, where, orderBy } = params;
    return this.prisma.acceptType.findMany({
      skip,
      take,
      cursor,
      where,
      orderBy,
    });
  }

  async createAcceptType(
    data: Prisma.AcceptTypeCreateInput,
  ): Promise<AcceptType> {
    return this.prisma.acceptType.create({ data });
  }

  async updateAcceptType(params: {
    where: Prisma.AcceptTypeWhereUniqueInput;
    data: Prisma.AcceptTypeUpdateInput;
  }): Promise<AcceptType> {
    const { where, data } = params;
    return this.prisma.acceptType.update({ data, where });
  }
}
