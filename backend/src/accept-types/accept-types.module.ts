import { Module } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { MessagesModule } from '../messages/messages.module';
import { AcceptTypesController } from './accept-types.controller';
import { AcceptTypesService } from './accept-types.service';

@Module({
  imports: [DatabaseModule, MessagesModule],
  controllers: [AcceptTypesController],
  providers: [AcceptTypesService],
  exports: [AcceptTypesService],
})
export class AcceptTypesModule {}
