import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';
import { JobsModule } from '../jobs/jobs.module';
import { MessagesModule } from '../messages/messages.module';
import { ApiService } from './api.service';

@Module({
  imports: [HttpModule, ConfigModule, JobsModule, MessagesModule],
  providers: [ApiService],
  exports: [ApiService],
})
export class ApiModule {}
