import { Injectable, OnModuleInit } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { OnEvent } from '@nestjs/event-emitter';
import { Timeout } from '@nestjs/schedule';
import { Job, JobStatus } from '@prisma/client';
import { instanceToPlain } from 'class-transformer';
import { firstValueFrom } from 'rxjs';
import { JobsService } from '../jobs/jobs.service';
import { JobDto } from '../jobs/jobs.transfer';
import { MessagesService } from '../messages/messages.service';

@Injectable()
export class ApiService implements OnModuleInit {
  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
    private readonly jobsService: JobsService,
    private readonly messagesService: MessagesService,
  ) {}

  async onModuleInit() {
    const jobs = await this.getRunningJobs();
    for (const job of jobs) {
      await this.jobsService.updateJob({
        where: { id: job.id },
        data: { status: JobStatus.TERMINATED },
      });
    }
  }

  get url() {
    return this.configService.get('MIA_API_URL');
  }

  @Timeout(5000)
  @OnEvent('job.created')
  async createJob(job: Job) {
    if (job !== undefined && job.interactive) {
      await this.invokeJob(job);
    } else {
      const jobs = await this.getRunningJobs();
      if (jobs.length === 0) {
        const queued = await this.getQueuedJob();
        if (queued !== null) {
          await this.invokeJob(queued);
        }
      }
    }
  }

  @OnEvent('job.finalized')
  async finalizeJob(job: Job) {
    if (job.status === JobStatus.TERMINATED) {
      await this.terminateJob(job);
    }

    const jobs = await this.getRunningJobs();
    if (jobs.length === 0) {
      const queued = await this.getQueuedJob();
      if (queued !== null) {
        await this.invokeJob(queued);
      }
    }
  }

  async getRunningJobs(): Promise<Job[]> {
    return this.jobsService.jobs({
      where: { interactive: false, status: JobStatus.PENDING },
    });
  }

  async getQueuedJob(): Promise<Job> {
    const jobs = await this.jobsService.jobs({
      where: { interactive: false, status: JobStatus.WAITING },
      skip: 0,
      take: 1,
      orderBy: { modified: 'asc' },
    });
    return jobs.length ? jobs[0] : null;
  }

  async invokeJob(pick: Job): Promise<Job> {
    try {
      const response = await firstValueFrom(
        this.httpService.post(
          `${this.url}/job`,
          instanceToPlain(new JobDto(pick)),
        ),
      );

      const job = await this.jobsService.updateJob({
        where: { id: pick.id },
        data: { status: JobStatus.PENDING },
      });
      await this.messagesService.createMessage({
        source: 'executor.job.invoke',
        content: JSON.stringify(instanceToPlain(new JobDto(job))),
      });
      await this.messagesService.createMessage({
        source: 'executor.job.invoke.response',
        content: JSON.stringify(response),
      });
      return job;
    } catch (ex) {
      await this.messagesService.createMessage({
        source: 'executor.job.invoke.exception',
        content: JSON.stringify(ex),
      });
      return null;
    }
  }

  async terminateJob(job: Job) {
    try {
      const response = firstValueFrom(
        this.httpService.delete(`${this.url}/job`, {
          data: instanceToPlain(new JobDto(job)),
        }),
      );

      await this.messagesService.createMessage({
        source: 'executor.job.terminate',
        content: JSON.stringify(instanceToPlain(new JobDto(job))),
      });
      await this.messagesService.createMessage({
        source: 'executor.job.terminate.response',
        content: JSON.stringify(response),
      });
    } catch (ex) {
      await this.messagesService.createMessage({
        source: 'executor.job.terminate.exception',
        content: JSON.stringify(ex),
      });
    }
  }
}
