import { Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { MINIO_CONNECTION } from 'nestjs-minio';
import { Client } from 'minio';
import { createHash } from 'crypto';
import { extname } from 'path';

@Injectable()
export class MinioClientService implements OnModuleInit {
  constructor(@Inject(MINIO_CONNECTION) private readonly client: Client) {}

  async onModuleInit() {
    await this.createBucket('media-files');
    await this.createBucket('result-files');
  }

  async createBucket(bucketName = 'default') {
    const exist = await this.client.bucketExists(bucketName);
    if (!exist) {
      await this.client.makeBucket(bucketName, 'us-east-1');
    }
  }

  async getUrl(objectName: string, bucketName = 'default') {
    await this.client.statObject(bucketName, objectName);
    return this.client.presignedUrl('GET', bucketName, objectName);
  }

  async upload(file: Express.Multer.File, bucketName = 'default') {
    const shaHash = createHash('sha256')
      .update(Date.now().toString())
      .digest('hex');
    const fileExt = extname(file.originalname);
    const objectName = `${shaHash}${fileExt}`;
    const metaData = { 'Content-Type': file.mimetype };
    await this.client.putObject(bucketName, objectName, file.buffer, metaData);
    return `/store/${bucketName}/${objectName}`;
  }
}
