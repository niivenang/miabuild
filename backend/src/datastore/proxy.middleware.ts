import { Injectable, NestMiddleware } from '@nestjs/common';
import { createProxyMiddleware } from 'http-proxy-middleware';
import { MinioClientService } from './minio-client.service';

@Injectable()
export class ProxyMiddleware implements NestMiddleware {
  private proxy = createProxyMiddleware({
    changeOrigin: true,
    ignorePath: true,
    router: async (req) => {
      return this.minioClient.getUrl(
        req.params.objectName,
        req.params.bucketName,
      );
    },
  });

  constructor(private minioClient: MinioClientService) {}

  use(req: any, res: any, next: () => void) {
    this.proxy(req, res, next);
  }
}
