import {
  Body,
  ConflictException,
  Controller,
  Get,
  Post,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { MessagesService } from './messages.service';
import {
  CreateMessageDto,
  ListMessagesQueryDto,
  MessageDto,
} from './messages.transfer';

@ApiTags('messages')
@Controller('messages')
export class MessagesController {
  constructor(private readonly messagesService: MessagesService) {}

  @Get()
  async listMessages(
    @Query() params: ListMessagesQueryDto,
  ): Promise<MessageDto[]> {
    const { startDate, endDate, offset, limit } = params;
    const messages = await this.messagesService.messages({
      where: {
        AND: [{ created: { gte: startDate } }, { created: { lte: endDate } }],
      },
      skip: offset,
      take: limit,
      orderBy: { created: 'desc' },
    });
    return messages.map((message) => new MessageDto(message));
  }

  @Post()
  async createMessage(@Body() data: CreateMessageDto): Promise<MessageDto> {
    const message = await this.messagesService.createMessage(data);
    if (message === null) {
      throw new ConflictException();
    }
    return new MessageDto(message);
  }
}
