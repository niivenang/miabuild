import { Message } from '@prisma/client';
import { Exclude, Expose, Type } from 'class-transformer';
import { IsDate, IsDefined, IsJSON, IsNumber, IsString } from 'class-validator';

export class ListMessagesQueryDto {
  @Expose({ name: 'start-date' })
  @Type(() => Date)
  @IsDate()
  startDate: Date;

  @Expose({ name: 'end-date' })
  @Type(() => Date)
  @IsDate()
  endDate: Date;

  @Type(() => Number)
  @IsNumber()
  offset = 0;

  @Type(() => Number)
  @IsNumber()
  limit = 100;
}

export class CreateMessageDto {
  @IsString()
  source: string;

  @IsDefined()
  @IsJSON()
  content: string;
}

export class MessageDto {
  @Exclude()
  id: string;

  source: string;

  content: string;

  @Expose({ toPlainOnly: true })
  created: Date;

  constructor(partial: Partial<Message>) {
    Object.assign(this, partial);
  }
}
