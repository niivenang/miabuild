import { Injectable, OnModuleInit } from '@nestjs/common';
import { Settings } from '@prisma/client';
import { generateKeyPairSync } from 'crypto';
import { PrismaService } from './prisma.service';

@Injectable()
export class SettingsService implements OnModuleInit {
  constructor(private prisma: PrismaService) {}

  async onModuleInit() {
    const settings = await this.settings();
    if (settings === null) {
      await this.createSettings();
    }
  }

  async settings(): Promise<Settings> {
    return this.prisma.settings.findUnique({ where: { id: 0 } });
  }

  async createSettings(): Promise<Settings> {
    const { publicKey, privateKey } = generateKeyPairSync('rsa', {
      modulusLength: 4096,
      publicKeyEncoding: { type: 'spki', format: 'pem' },
      privateKeyEncoding: {
        type: 'pkcs8',
        format: 'pem',
      },
    });
    return this.prisma.settings.create({
      data: { id: 0, initialized: true, publicKey, privateKey },
    });
  }

  async initialize(): Promise<Settings> {
    const { publicKey, privateKey } = generateKeyPairSync('rsa', {
      modulusLength: 4096,
      publicKeyEncoding: { type: 'spki', format: 'pem' },
      privateKeyEncoding: {
        type: 'pkcs8',
        format: 'pem',
      },
    });
    return this.prisma.settings.update({
      data: { initialized: true, publicKey, privateKey },
      where: { id: 0 },
    });
  }

  async updateSettings(data: { useBatch?: boolean }): Promise<Settings> {
    const { useBatch } = data;
    return this.prisma.settings.update({
      data: { useBatch },
      where: { id: 0 },
    });
  }
}
