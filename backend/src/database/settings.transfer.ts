import { Settings } from '@prisma/client';
import { Exclude, Expose } from 'class-transformer';
import { IsBoolean } from 'class-validator';

export class UpdateSettingsDto {
  @Expose({ name: 'use_batch' })
  @IsBoolean()
  useBatch: boolean;
}

export class SettingsDto {
  @Exclude()
  id: number;

  @Expose({ name: 'public_key' })
  publicKey: string;

  @Exclude()
  privateKey: string;

  @Exclude()
  initialized: boolean;

  @Expose({ name: 'use_batch' })
  useBatch: boolean;

  @Expose({ toPlainOnly: true })
  created: Date;

  @Expose({ toPlainOnly: true })
  modified: Date;

  constructor(partial: Partial<Settings>) {
    Object.assign(this, partial);
  }
}
