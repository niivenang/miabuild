import { Injectable } from '@nestjs/common';
import { Result, Prisma } from '@prisma/client';
import { PrismaService } from '../database/prisma.service';

@Injectable()
export class ResultsService {
  constructor(private prisma: PrismaService) {}

  async results(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.ResultWhereUniqueInput;
    where?: Prisma.ResultWhereInput;
    orderBy?: Prisma.ResultOrderByWithRelationInput;
  }): Promise<Result[]> {
    const { skip, take, cursor, where, orderBy } = params;
    return this.prisma.result.findMany({
      skip,
      take,
      cursor,
      where,
      orderBy,
    });
  }

  async createResult(data: Prisma.ResultCreateInput): Promise<Result> {
    return this.prisma.result.create({ data });
  }
}
