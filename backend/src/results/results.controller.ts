import {
  BadRequestException,
  Body,
  ConflictException,
  Controller,
  Get,
  MessageEvent,
  Param,
  ParseUUIDPipe,
  Post,
  Query,
  Sse,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiTags } from '@nestjs/swagger';
import { ResultType } from '@prisma/client';
import { instanceToPlain } from 'class-transformer';
import { distinct, interval, map, switchMap, Observable } from 'rxjs';
import { MinioClientService } from '../datastore/minio-client.service';
import { MessagesService } from '../messages/messages.service';
import { ResultsService } from './results.service';
import {
  CreateResultFormDto,
  GetResultsQueryDto,
  ResultDto,
} from './results.transfer';

@ApiTags('results')
@Controller('results')
export class ResultsController {
  constructor(
    private readonly resultsService: ResultsService,
    private readonly messagesService: MessagesService,
    private minioClient: MinioClientService,
  ) {}

  @Get('jobs/:jid')
  async getResults(
    @Param('jid', ParseUUIDPipe) jid: string,
    @Query() params: GetResultsQueryDto,
  ): Promise<ResultDto[]> {
    const { resultType, offset, limit } = params;
    const results = await this.resultsService.results({
      where: { jid, type: resultType },
      skip: offset,
      take: limit,
      orderBy: { created: 'desc' },
    });
    return results.map((result) => new ResultDto(result));
  }

  @Post()
  @UseInterceptors(FileInterceptor('result_file'))
  async createResult(
    @Query('url') url: string,
    @Body() data: CreateResultFormDto,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<ResultDto> {
    if (data.type !== ResultType.JSON) {
      if (url === undefined && file === undefined) {
        throw new BadRequestException();
      }
    }

    if (file !== undefined) {
      try {
        const url = await this.minioClient.upload(file, 'result-files');
        const { jid, alias, target, type, content } = data;
        const result = await this.resultsService.createResult({
          job: { connect: { id: jid } },
          algorithm: { connect: { alias_target: { alias, target } } },
          type,
          url,
          content,
        });
        if (result === null) {
          throw new ConflictException();
        }

        await this.messagesService.createMessage({
          source: 'results.controller.create',
          content: JSON.stringify(instanceToPlain(new ResultDto(result))),
        });
        return new ResultDto(result);
      } catch (ex) {
        if (ex instanceof ConflictException) {
          throw ex;
        } else {
          throw new BadRequestException();
        }
      }
    } else {
      const { jid, alias, target, type, content } = data;
      const result = await this.resultsService.createResult({
        job: { connect: { id: jid } },
        algorithm: { connect: { alias_target: { alias, target } } },
        type,
        url,
        content,
      });
      if (result === null) {
        throw new ConflictException();
      }

      await this.messagesService.createMessage({
        source: 'results.controller.create',
        content: JSON.stringify(instanceToPlain(new ResultDto(result))),
      });
      return new ResultDto(result);
    }
  }

  @Sse('sse/:jid')
  sse(@Param('jid', ParseUUIDPipe) jid: string): Observable<MessageEvent> {
    return interval(1000).pipe(
      switchMap(() => {
        return this.resultsService.results({
          where: { jid },
          orderBy: { created: 'desc' },
        });
      }),
      distinct((results) => results.length),
      map((results) => ({
        data: {
          sent: new Date(),
          results: instanceToPlain(
            results.map((result) => new ResultDto(result)),
          ),
        },
      })),
    );
  }
}
