import { Result, ResultType, Target } from '@prisma/client';
import { Exclude, Expose, Transform, Type } from 'class-transformer';
import {
  IsDefined,
  IsIn,
  IsJSON,
  IsNotEmpty,
  IsNumber,
  IsString,
  IsUUID,
} from 'class-validator';
import * as ResultTypeTransform from '../helpers/result-type.pipe';
import * as TargetTransform from '../helpers/target.pipe';

export class GetResultsQueryDto {
  @Expose({ name: 'result-type' })
  @Transform(ResultTypeTransform.toClass, { toClassOnly: true })
  @Transform(ResultTypeTransform.toPlain, { toPlainOnly: true })
  @IsIn(ResultTypeTransform.getOptions())
  resultType: ResultType;

  @Type(() => Number)
  @IsNumber()
  offset = 0;

  @Type(() => Number)
  @IsNumber()
  limit = 100;
}

export class CreateResultFormDto {
  @Expose({ name: 'job_id' })
  @IsDefined()
  @IsUUID()
  jid: string;

  @IsDefined()
  @IsNotEmpty()
  @IsString()
  alias: string;

  @Transform(TargetTransform.toClass, { toClassOnly: true })
  @Transform(TargetTransform.toPlain, { toPlainOnly: true })
  @IsDefined()
  @IsIn(TargetTransform.getOptions())
  target: Target;

  @Expose({ name: 'result_type' })
  @Transform(ResultTypeTransform.toClass, { toClassOnly: true })
  @Transform(ResultTypeTransform.toPlain, { toPlainOnly: true })
  @IsDefined()
  @IsIn(ResultTypeTransform.getOptions())
  type: ResultType;

  @IsJSON()
  content: string;
}

export class ResultDto {
  @Exclude()
  id: string;

  @Expose({ name: 'job_id' })
  jid: string;

  alias: string;

  @Transform(TargetTransform.toClass, { toClassOnly: true })
  @Transform(TargetTransform.toPlain, { toPlainOnly: true })
  target: Target;

  @Expose({ name: 'result_type' })
  @Transform(ResultTypeTransform.toClass, { toClassOnly: true })
  @Transform(ResultTypeTransform.toPlain, { toPlainOnly: true })
  type: ResultType;

  @Expose({ toPlainOnly: true })
  url: string;

  content: string;

  @Expose({ toPlainOnly: true })
  created: Date;

  constructor(partial: Partial<Result>) {
    Object.assign(this, partial);
  }
}
