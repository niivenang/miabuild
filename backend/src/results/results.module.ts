import { Module } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { DatastoreModule } from '../datastore/datastore.module';
import { MessagesModule } from '../messages/messages.module';
import { ResultsController } from './results.controller';
import { ResultsService } from './results.service';

@Module({
  imports: [DatabaseModule, MessagesModule, DatastoreModule],
  controllers: [ResultsController],
  providers: [ResultsService],
})
export class ResultsModule {}
