--
-- PostgreSQL database dump
--

-- Dumped from database version 15.1 (Debian 15.1-1.pgdg110+1)
-- Dumped by pg_dump version 15.3 (Ubuntu 15.3-1.pgdg22.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

-- *not* creating schema, since initdb creates it


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS '';


--
-- Name: job_status; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.job_status AS ENUM (
    'waiting',
    'pending',
    'completed',
    'terminated'
);


ALTER TYPE public.job_status OWNER TO postgres;

--
-- Name: media_type; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.media_type AS ENUM (
    'image',
    'audio',
    'video'
);


ALTER TYPE public.media_type OWNER TO postgres;

--
-- Name: result_type; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.result_type AS ENUM (
    'image',
    'audio',
    'video',
    'json'
);


ALTER TYPE public.result_type OWNER TO postgres;

--
-- Name: target; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.target AS ENUM (
    'dev',
    'cpu',
    'gpu',
    'batch',
    'synthesis',
    'external'
);


ALTER TYPE public.target OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: Settings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Settings" (
    id integer NOT NULL,
    public_key text NOT NULL,
    private_key text NOT NULL,
    initialized boolean DEFAULT false NOT NULL,
    use_batch_algorithms boolean DEFAULT true NOT NULL,
    created timestamp(3) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    modified timestamp(3) without time zone NOT NULL
);


ALTER TABLE public."Settings" OWNER TO postgres;

--
-- Name: accept_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.accept_types (
    suffix text NOT NULL,
    type public.media_type NOT NULL,
    enabled boolean DEFAULT true NOT NULL,
    created timestamp(3) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    modified timestamp(3) without time zone NOT NULL
);


ALTER TABLE public.accept_types OWNER TO postgres;

--
-- Name: algorithms; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.algorithms (
    alias text NOT NULL,
    target public.target DEFAULT 'dev'::public.target NOT NULL,
    name text NOT NULL,
    "order" integer DEFAULT 1 NOT NULL,
    description text,
    badge_description text,
    types public.media_type[],
    inputs xml,
    url text,
    enabled boolean DEFAULT true NOT NULL,
    created timestamp(3) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    modified timestamp(3) without time zone NOT NULL
);


ALTER TABLE public.algorithms OWNER TO postgres;

--
-- Name: jobs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jobs (
    id text NOT NULL,
    media_file_id text NOT NULL,
    parameters json,
    interactive boolean DEFAULT true NOT NULL,
    status public.job_status DEFAULT 'waiting'::public.job_status NOT NULL,
    labels text[],
    created timestamp(3) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    modified timestamp(3) without time zone NOT NULL
);


ALTER TABLE public.jobs OWNER TO postgres;

--
-- Name: jobs_algorithms; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jobs_algorithms (
    job_id text NOT NULL,
    algorithm_alias text NOT NULL,
    algorithm_target public.target NOT NULL,
    created timestamp(3) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.jobs_algorithms OWNER TO postgres;

--
-- Name: media_files; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.media_files (
    id text NOT NULL,
    type public.media_type NOT NULL,
    url text NOT NULL,
    path text,
    labels text[],
    created timestamp(3) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    modified timestamp(3) without time zone NOT NULL
);


ALTER TABLE public.media_files OWNER TO postgres;

--
-- Name: messages; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.messages (
    id text NOT NULL,
    source text,
    content json NOT NULL,
    created timestamp(3) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.messages OWNER TO postgres;

--
-- Name: results; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.results (
    id text NOT NULL,
    job_id text NOT NULL,
    algorithm_alias text NOT NULL,
    algorithm_target public.target NOT NULL,
    type public.result_type NOT NULL,
    url text,
    content json,
    created timestamp(3) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.results OWNER TO postgres;

--
-- Data for Name: Settings; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Settings" (id, public_key, private_key, initialized, use_batch_algorithms, created, modified) FROM stdin;
\.


--
-- Data for Name: accept_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.accept_types (suffix, type, enabled, created, modified) FROM stdin;
\.


--
-- Data for Name: algorithms; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.algorithms (alias, target, name, "order", description, badge_description, types, inputs, url, enabled, created, modified) FROM stdin;
\.


--
-- Data for Name: jobs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jobs (id, media_file_id, parameters, interactive, status, labels, created, modified) FROM stdin;
\.


--
-- Data for Name: jobs_algorithms; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jobs_algorithms (job_id, algorithm_alias, algorithm_target, created) FROM stdin;
\.


--
-- Data for Name: media_files; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.media_files (id, type, url, path, labels, created, modified) FROM stdin;
\.


--
-- Data for Name: messages; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.messages (id, source, content, created) FROM stdin;
\.


--
-- Data for Name: results; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.results (id, job_id, algorithm_alias, algorithm_target, type, url, content, created) FROM stdin;
\.


--
-- Name: Settings Settings_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Settings"
    ADD CONSTRAINT "Settings_pkey" PRIMARY KEY (id);


--
-- Name: jobs_algorithms jobs_algorithms_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jobs_algorithms
    ADD CONSTRAINT jobs_algorithms_pkey PRIMARY KEY (job_id, algorithm_alias, algorithm_target);


--
-- Name: jobs jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jobs
    ADD CONSTRAINT jobs_pkey PRIMARY KEY (id);


--
-- Name: media_files media_files_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.media_files
    ADD CONSTRAINT media_files_pkey PRIMARY KEY (id);


--
-- Name: messages messages_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (id);


--
-- Name: results results_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.results
    ADD CONSTRAINT results_pkey PRIMARY KEY (id);


--
-- Name: accept_types_suffix_key; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX accept_types_suffix_key ON public.accept_types USING btree (suffix);


--
-- Name: algorithms_alias_target_key; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX algorithms_alias_target_key ON public.algorithms USING btree (alias, target);


--
-- Name: jobs_algorithms jobs_algorithms_algorithm_alias_algorithm_target_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jobs_algorithms
    ADD CONSTRAINT jobs_algorithms_algorithm_alias_algorithm_target_fkey FOREIGN KEY (algorithm_alias, algorithm_target) REFERENCES public.algorithms(alias, target) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: jobs_algorithms jobs_algorithms_job_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jobs_algorithms
    ADD CONSTRAINT jobs_algorithms_job_id_fkey FOREIGN KEY (job_id) REFERENCES public.jobs(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: jobs jobs_media_file_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jobs
    ADD CONSTRAINT jobs_media_file_id_fkey FOREIGN KEY (media_file_id) REFERENCES public.media_files(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: results results_algorithm_alias_algorithm_target_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.results
    ADD CONSTRAINT results_algorithm_alias_algorithm_target_fkey FOREIGN KEY (algorithm_alias, algorithm_target) REFERENCES public.algorithms(alias, target) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: results results_job_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.results
    ADD CONSTRAINT results_job_id_fkey FOREIGN KEY (job_id) REFERENCES public.jobs(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;


--
-- PostgreSQL database dump complete
--

