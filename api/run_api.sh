#!/usr/bin/env bash

python -m gunicorn -b 0.0.0.0:8000 -k gevent --chdir app \
    --log-level ${MIA_API_LOGLEVEL:-info} main:app
