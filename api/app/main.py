#!/usr/bin/env python3

from flask import Flask


def create_app():
    app = Flask(__name__)
    from app.job import bp
    app.register_blueprint(bp)
    return app


app = create_app()
