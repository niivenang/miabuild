#!/usr/bin/env python3

from typing import Any, Dict, Optional

from flask import Blueprint, request, abort, jsonify

from mia.task.app import TaskControl
from mia.task import mia_app

bp = Blueprint("job", __name__, url_prefix="/job")


@bp.route("", methods=["POST"])
def invoke_job():
    data: Optional[Dict[str, Any]] = request.get_json()
    if data is None:
        abort(400)
    tasks = TaskControl.invoke_job(mia_app, data)
    return jsonify([f"{t.alias}@{t.target}" for t in tasks])


@bp.route("", methods=["DELETE"])
def terminate_job():
    data: Optional[Dict[str, Any]] = request.get_json()
    if data is None or "id" not in data:
        abort(400)
    tasks = TaskControl.terminate_job(mia_app, data["id"])
    return jsonify(tasks)
