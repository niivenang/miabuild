#!/usr/bin/env python3
# pylint: skip-file

import time

from mia.task import app, JobTask


@app.task(name="algorithm.sleep_5s")
def sleep_5s(t, _):
    task = JobTask(t)
    task.publish_message("sleep_5s() started!")
    time.sleep(5)
    task.publish_message("sleep_5s() ended!")


@app.task(name="algorithm.sleep_10s")
def sleep_10s(t, _):
    task = JobTask(t)
    task.publish_message("sleep_10s() started!")
    time.sleep(10)
    task.publish_message("sleep_10s() ended!")


@app.task(name="algorithm.sleep_100s")
def sleep_100s(t, _):
    task = JobTask(t)
    task.publish_message("sleep_100s() started!")
    time.sleep(100)
    task.publish_message("sleep_100s() ended!")
