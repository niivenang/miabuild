#!/usr/bin/env python3
# pylint: skip-file

import time

import pytest
import requests

from mia.task.settings import BACKEND_URL
from app.main import create_app


@pytest.fixture
def app():
    app = create_app()
    yield app


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture()
def create_algorithms():
    for name in ["sleep_5s", "sleep_10s", "sleep_100s"]:
        resp = requests.get(f"{BACKEND_URL}/algorithms/{name}", timeout=10)
        if resp.status_code == 404:
            algorithm = {
                "alias": f"{name}",
                "target": "dev",
                "name": f"{name}@dev",
                "enable": False
            }
            resp = requests.post(f"{BACKEND_URL}/algorithms",
                                 json=algorithm,
                                 timeout=10)
            assert resp.status_code == 201


@pytest.fixture()
def create_invoke_job(create_algorithms):
    media_file = {"media_file": open("tests/audio.wav", "rb")}
    media_file_data = {"media_type": "audio", "path": "tests/audio.wav"}
    resp = requests.post(f"{BACKEND_URL}/media-files",
                         files=media_file,
                         data=media_file_data,
                         timeout=10)
    assert resp.status_code == 201
    file_id = resp.json()["id"]

    job = {"file_id": file_id, "algorithms": []}
    job["algorithms"].append({"alias": "sleep_5s", "target": "dev"})
    job["algorithms"].append({"alias": "sleep_10s", "target": "dev"})
    resp = requests.post(f"{BACKEND_URL}/jobs", json=job, timeout=10)
    assert resp.status_code == 201
    return resp.json()


def test_invoke_job(create_invoke_job, client):
    resp = client.post("/job", json=create_invoke_job)
    assert resp.status_code == 200
    assert len(resp.json) == 2
    time.sleep(20)


@pytest.fixture()
def create_terminate_job(create_algorithms):
    media_file = {"media_file": open("tests/audio.wav", "rb")}
    media_file_data = {"media_type": "audio", "path": "tests/audio.wav"}
    resp = requests.post(f"{BACKEND_URL}/media-files",
                         files=media_file,
                         data=media_file_data,
                         timeout=10)
    assert resp.status_code == 201
    file_id = resp.json()["id"]

    job = {"file_id": file_id, "algorithms": []}
    job["algorithms"].append({"alias": "sleep_10s", "target": "dev"})
    job["algorithms"].append({"alias": "sleep_100s", "target": "dev"})
    resp = requests.post(f"{BACKEND_URL}/jobs", json=job, timeout=10)
    assert resp.status_code == 201
    return resp.json()


def test_terminate_job(create_terminate_job, client):
    resp = client.post("/job", json=create_terminate_job)
    assert resp.status_code == 200
    assert len(resp.json) == 2
    time.sleep(30)
    resp = client.delete("/job", json=create_terminate_job)
    assert resp.status_code == 200
    assert len(resp.json) == 1
