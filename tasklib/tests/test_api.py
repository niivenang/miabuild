#!/usr/bin/env python3
# pylint: skip-file

import shutil

import pytest
import requests

from mia.task.app import CeleryApp, TaskControl
from mia.task.settings import BACKEND_URL
from mia.task import JobTask


@pytest.fixture(scope="session")
def celery_config():
    return {
        "task_serializer": "miajson",
        "accept_content": ["application/json"],
        "task_default_queue": "dev",
        "task_always_eager": True
    }


@pytest.fixture(scope="session")
def create_algorithms():
    for name in [
            "publish_message", "has_asset_input", "publish_result_file",
            "publish_result_dict"
    ]:
        resp = requests.get(f"{BACKEND_URL}/algorithms/{name}", timeout=10)
        if resp.status_code == 404:
            algorithm = {
                "alias": f"{name}",
                "target": "dev",
                "name": f"{name}@dev",
                "enable": False
            }
            resp = requests.post(f"{BACKEND_URL}/algorithms",
                                 json=algorithm,
                                 timeout=10)
            assert resp.status_code == 201


@pytest.fixture(scope="session")
def create_invoke_job(create_algorithms):
    media_file = {"media_file": open("tests/audio.wav", "rb")}
    media_file_data = {"media_type": "audio", "path": "tests/audio.wav"}
    resp = requests.post(f"{BACKEND_URL}/media-files",
                         files=media_file,
                         data=media_file_data,
                         timeout=10)
    assert resp.status_code == 201
    file_id = resp.json()["id"]

    job = {"file_id": file_id, "algorithms": []}
    job["algorithms"].append({"alias": "publish_message", "target": "dev"})
    job["algorithms"].append({"alias": "has_asset_input", "target": "dev"})
    job["algorithms"].append({"alias": "publish_result_file", "target": "dev"})
    job["algorithms"].append({"alias": "publish_result_dict", "target": "dev"})
    resp = requests.post(f"{BACKEND_URL}/jobs", json=job, timeout=10)
    assert resp.status_code == 201
    return resp.json()


def test_invoke_job(create_invoke_job, celery_app, celery_worker):

    @celery_app.task(name="algorithm.has_asset_input")
    def has_asset_input(t, p):
        assert isinstance(p, dict)
        task = JobTask(t)
        assetfile = task.assetdir / "image.png"
        content = {
            "assetfile_path": str(assetfile),
            "assetfile_exists": assetfile.exists(),
            "infile_path": str(task.infile),
            "infile_exists":
            False if task.infile is None else task.infile.exists()
        }
        task.publish_message(content)

    @celery_app.task(name="algorithm.publish_message")
    def publish_message(t, _):
        task = JobTask(t)
        task.publish_message("publish_message() is invoked!")

    @celery_app.task(name="algorithm.publish_result_file")
    def publish_result_file(t, _):
        task = JobTask(t, app=app)
        infile = task.assetdir / "video.mp4"
        outfile = task.outdir / "video.mp4"
        shutil.copy(infile, outfile)
        task.publish_result(outfile, "video",
                            "publish_result_file() is invoked!")

    @celery_app.task(name="algorithm.publish_result_dict")
    def publish_result_dict(t, _):
        task = JobTask(t, app=app)
        infile = task.assetdir / "image.png"
        outfile = task.outdir / "image.png"
        shutil.copy(infile, outfile)
        results = {"path": outfile, "name": outfile.name, "score": 10.0}
        task.publish_result_dict(results, "publish_result_dict() is invoked!")

    celery_worker.reload()
    app = CeleryApp(celery=celery_app)
    TaskControl.invoke_job(app, create_invoke_job)
