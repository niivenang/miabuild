#!/usr/bin/env python3
"""
Default settings for Job Task.
"""

from pathlib import Path
import os

BACKEND_URL = os.getenv("MIA_BACKEND_URL", "http://localhost:3000")
ASSET_FOLDER = Path(os.getenv("MIA_ASSET_FOLDER", "assets/"))
WIP_FOLDER = Path(os.getenv("MIA_WIP_FOLDER", "wip/"))

REDIS_URL = os.getenv("REDIS_URL", "redis://localhost:6379")

MINIO_ENDPOINT = os.getenv("MINIO_ENDPOINT", "localhost")
MINIO_PORT = int(os.getenv("MINIO_PORT", "9000"))
MINIO_ACCESS_KEY = os.getenv("MINIO_ACCESS_KEY", "minioadmin")
MINIO_SECRET_KEY = os.getenv("MINIO_SECRET_KEY", "minioadmin")
