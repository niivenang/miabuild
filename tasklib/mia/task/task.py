#!/usr/bin/env python3
"""
Task for Job Task.
"""

from dataclasses import dataclass
from typing import Any, Dict, Optional, Tuple
import json


@dataclass
class Task:
    """Task information."""
    _jid: str
    _file: str
    _params: Dict[str, str]
    _alias: str
    _target: str

    @property
    def jid(self) -> str:  # pylint: disable=invalid-name
        """Return the job identifier."""
        return self._jid

    @property
    def file(self) -> str:
        """Return the input file."""
        return self._file

    @property
    def params(self) -> Dict[str, str]:
        """Return the parameters."""
        return self._params

    @property
    def alias(self) -> str:
        """Return alias of the algorithm."""
        return self._alias

    @property
    def target(self) -> str:
        """Return target of the algorithm."""
        return self._target

    @property
    def algorithm(self) -> Tuple[str, str]:
        """Return the algorithm."""
        return (self._alias, self._target)

    @classmethod
    def create(cls,
               json_data: Dict[str, Any],
               idx: int = 0) -> Optional["Task"]:
        """Return an instance derived from a job in the backend."""
        if not isinstance(json_data, dict) or \
            "id" not in json_data or "parameters" not in json_data or \
            "file" not in json_data or "algorithms" not in json_data:
            return None
        if not isinstance(json_data["file"], dict) or \
            "url" not in json_data["file"]:
            return None
        if not isinstance(json_data["algorithms"], list) or \
            idx >= len(json_data["algorithms"]):
            return None
        if not isinstance(json_data["algorithms"][idx], dict) or \
            "alias" not in json_data["algorithms"][idx] or \
            "target" not in json_data["algorithms"][idx]:
            return None

        params = {}
        if json_data["parameters"] is not None:
            try:
                params = json.loads(json_data["parameters"])
            except json.JSONDecodeError:
                params = {}

        return Task(json_data["id"], json_data["file"]["url"], params,
                    json_data["algorithms"][idx]["alias"],
                    json_data["algorithms"][idx]["target"])
