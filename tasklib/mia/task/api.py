#!/usr/bin/env python3
"""
API for Job Task.
"""

from pathlib import Path
from typing import Any, Dict, Optional
import shutil
import tempfile

import requests

from .app import Message, CeleryApp
from .json import json
from .task import Task

mia_app = CeleryApp()


class JobTask:
    """
    A class to faciliate communication between the backend and
    the job task.
    """

    def __init__(self, task: Task, *, app: Optional[CeleryApp] = None):
        self._app = mia_app if app is None else app
        self._task = task

        self._in_dir = Path(tempfile.mkdtemp(dir=self._app.wip_dir))
        self._out_dir = Path(tempfile.mkdtemp(dir=self._app.wip_dir))
        self._in_file: Optional[Path] = None
        self.initialize_task()

    def __del__(self):
        self.finalize_task()

    def initialize_task(self):
        """Perform initialization routine for the job task."""
        if (file_id :=
                self._app.get_datastore_id(self._task.file)) is not None:
            bucket_name, object_name = file_id
            self._in_file = self._app.download_file(object_name,
                                                    self._in_dir,
                                                    bucket_name=bucket_name)

    def finalize_task(self):
        """Perform finalization routine for the job task."""
        shutil.rmtree(self._in_dir, ignore_errors=True)
        shutil.rmtree(self._out_dir, ignore_errors=True)

    @property
    def assetdir(self) -> Path:
        """Return the asset directory."""
        return self._app.asset_dir

    @property
    def indir(self) -> Path:
        """Return the input directory."""
        return self._in_dir

    @property
    def outdir(self) -> Path:
        """Return the output directory."""
        return self._out_dir

    @property
    def infile(self) -> Optional[Path]:
        """Return the input file."""
        return self._in_file

    def download_file(self, url: str) -> Optional[Path]:
        """Download a file from the datastore."""
        if (file_id := self._app.get_datastore_id(url)) is not None:
            bucket_name, object_name = file_id
            return self._app.download_file(object_name,
                                           self._in_dir,
                                           bucket_name=bucket_name)
        return None

    def publish_message(self, message: Message):
        """Publish a message to the backend."""
        name = "@".join(self._task.algorithm)
        self._app.publish_message(message, f"{self._task.jid}#{name}")

    def publish_result(self,
                       result: Path,
                       mediatype: str,
                       description: Optional[str] = None):
        """Publish result file to the backend."""
        if not isinstance(result, Path):
            return
        if not isinstance(mediatype, str):
            return
        if mediatype not in ["image", "audio", "video"]:
            return
        file_url = self._app.upload_file(result)
        content = {"mia.path": str(result)}
        if isinstance(description, str):
            content["mia.description"] = description
        payload = {
            "job_id": self._task.jid,
            "alias": self._task.algorithm[0],
            "target": self._task.algorithm[1],
            "result_type": mediatype,
            "content": json.dumps(content)
        }
        requests.post(f"{self._app.backend_url}/results?url={file_url}",
                      json=payload,
                      timeout=10)

    def publish_result_dict(self,
                            results: Dict[str, Any],
                            description: Optional[str] = None):
        """Publish result dictionary to the backend."""
        if not isinstance(results, dict):
            return
        keys = list(results.keys())
        for key in keys:
            if isinstance(results[key], Path):
                file_url = self._app.upload_file(results[key])
                results[f"mia.path.{key}"] = str(results[key])
                results[key] = file_url
        if isinstance(description, str):
            results["mia.description"] = description
        payload = {
            "job_id": self._task.jid,
            "alias": self._task.algorithm[0],
            "target": self._task.algorithm[1],
            "result_type": "json",
            "content": json.dumps(results)
        }
        requests.post(f"{self._app.backend_url}/results",
                      json=payload,
                      timeout=10)
