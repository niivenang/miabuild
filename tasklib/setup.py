#!/usr/bin/env python3
# pylint: skip-file

import setuptools

with open("README.md", "r", encoding="utf-8") as fd:
    long_description = fd.read()

setuptools.setup(
    name="mia-tasklib",
    version="1.0.8",
    author="Aaron Yong Yong Cheng",
    author_email="yyongche@dso.org.sg",
    description="API for Job Task",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/yyongche/miabuild/src/master/tasklib/",
    packages=setuptools.find_packages(),
    install_requires=["celery", "redis", "minio", "requests", "superjson"],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent"
    ],
    python_requires=">=3.8")
