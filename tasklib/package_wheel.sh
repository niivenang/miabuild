#!/usr/bin/env bash

pipenv run python setup.py clean --all
rm -rf build/ dist/ mia_tasklib.egg-info/
pipenv run python setup.py bdist_wheel

whlfile=`ls -1 dist/mia_tasklib-*-py3-none-any.whl | cut -c6-`
targetdirs=("../api" "../algo-base" "../algo-gpu")
if [ -f dist/${whlfile} ]; then
    for targetdir in ${targetdirs[@]}
    do
        if [ ! -f ${targetdir}/${whlfile} ]; then
            owhlfile=`ls -1 ${targetdir}/mia_tasklib-*-py3-none-any.whl`
            cp dist/${whlfile} ${targetdir}/${whlfile}
            pushd ${targetdir}
            if [ -d .venv ]; then
                pipenv uninstall mia_tasklib
                pipenv install ${whlfile}
            fi
            popd
            rm ${owhlfile}
        fi
    done
fi
