#!/usr/bin/env bash

pipenv requirements > requirements-tasklib.txt
cp requirements-tasklib.txt ../algo-gpu/

pipenv requirements --dev-only > requirements-dev.txt
cp requirements-dev.txt ../algo-gpu/
