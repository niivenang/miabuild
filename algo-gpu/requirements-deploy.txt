-i https://pypi.org/simple
amqp==5.1.1
appdirs==1.4.4
async-timeout==4.0.2
audioread==3.0.0 ; python_version >= '3.6'
billiard==3.6.4.0
celery==5.2.7
certifi==2022.12.7
cffi==1.15.1
charset-normalizer==3.1.0
click==8.1.3
click-didyoumean==0.3.0
click-plugins==1.1.1
click-repl==0.2.0
cmake==3.26.4
contourpy==1.1.0 ; python_version >= '3.8'
cupy-cuda116==10.6.0
cycler==0.11.0 ; python_version >= '3.6'
decorator==5.1.1 ; python_version >= '3.5'
dlib==19.24.2
face-alignment==1.4.0
fastrlock==0.8.1
ffmpeg==1.4
ffprobe==0.5
filelock==3.12.2 ; python_version >= '3.7'
fonttools==4.41.0 ; python_version >= '3.8'
idna==3.4
imageio==2.31.1 ; python_version >= '3.7'
imageio-ffmpeg==0.4.8
importlib-metadata==6.8.0 ; python_version < '3.9'
importlib-resources==6.0.0 ; python_version < '3.10'
jinja2==3.1.2 ; python_version >= '3.7'
joblib==1.3.1 ; python_version >= '3.7'
kiwisolver==1.4.4 ; python_version >= '3.7'
kombu==5.2.4
lazy-loader==0.3 ; python_version >= '3.7'
librosa==0.10.0.post2
lit==16.0.6
llvmlite==0.40.1 ; python_version >= '3.8'
markupsafe==2.1.3 ; python_version >= '3.7'
matplotlib==3.7.2
./mia_tasklib-1.0.8-py3-none-any.whl
minio==7.1.14
mlxtend==0.19.0
mpmath==1.3.0
msgpack==1.0.5
networkx==3.1 ; python_version >= '3.8'
numba==0.57.1 ; python_version >= '3.8'
numpy==1.24.4 ; python_version >= '3.8'
nvidia-cublas-cu11==11.10.3.66 ; platform_system == 'Linux' and platform_machine == 'x86_64'
nvidia-cuda-cupti-cu11==11.7.101 ; platform_system == 'Linux' and platform_machine == 'x86_64'
nvidia-cuda-nvrtc-cu11==11.7.99 ; platform_system == 'Linux' and platform_machine == 'x86_64'
nvidia-cuda-runtime-cu11==11.7.99 ; platform_system == 'Linux' and platform_machine == 'x86_64'
nvidia-cudnn-cu11==8.5.0.96 ; platform_system == 'Linux' and platform_machine == 'x86_64'
nvidia-cufft-cu11==10.9.0.58 ; platform_system == 'Linux' and platform_machine == 'x86_64'
nvidia-curand-cu11==10.2.10.91 ; platform_system == 'Linux' and platform_machine == 'x86_64'
nvidia-cusolver-cu11==11.4.0.1 ; platform_system == 'Linux' and platform_machine == 'x86_64'
nvidia-cusparse-cu11==11.7.4.91 ; platform_system == 'Linux' and platform_machine == 'x86_64'
nvidia-nccl-cu11==2.14.3 ; platform_system == 'Linux' and platform_machine == 'x86_64'
nvidia-nvtx-cu11==11.7.91 ; platform_system == 'Linux' and platform_machine == 'x86_64'
opencv-python==4.8.0.74
packaging==23.1 ; python_version >= '3.7'
pandas==1.5.3
pillow==8.4.0
pooch==1.6.0 ; python_version >= '3.6'
prompt-toolkit==3.0.38
protobuf==4.23.4 ; python_version >= '3.7'
pycparser==2.21
pyparsing==3.0.9 ; python_full_version >= '3.6.8'
python-dateutil==2.8.2 ; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
pytz==2023.3
pywavelets==1.4.1 ; python_version >= '3.8'
pyyaml==6.0
redis==4.5.4
requests==2.28.2
scikit-fuzzy==0.4.2
scikit-image==0.19.3
scikit-learn==0.24.2
scipy==1.10.1 ; python_version < '3.12' and python_version >= '3.8'
setuptools==68.0.0 ; python_version >= '3.7'
six==1.16.0
soundfile==0.12.1
sox==1.4.1
soxr==0.3.5 ; python_version >= '3.6'
superjson==1.0.2
sympy==1.12 ; python_version >= '3.8'
tensorboardx==2.6.1
threadpoolctl==3.2.0 ; python_version >= '3.8'
tifffile==2023.7.10 ; python_version >= '3.8'
torch==2.0.1 ; python_full_version >= '3.8.0'
torchsummary==1.5.1
tqdm==4.65.0
triton==2.0.0 ; platform_system == 'Linux' and platform_machine == 'x86_64'
ttictoc==0.5.6
typing-extensions==4.7.1 ; python_version >= '3.7'
urllib3==1.26.15
vine==5.0.0
wcwidth==0.2.6
wheel==0.40.0 ; python_version >= '3.7'
zipp==3.16.1 ; python_version < '3.10'
