#!/usr/bin/env bash

if [ -d .venv ]; then
    pipenv --rm
    rm -rf .venv/
fi
export PIPENV_VENV_IN_PROJECT=1
if [ -f Pipfile.lock ]; then
    pipenv --python 3.8
    pipenv run pip install nvidia-pyindex
    pipenv run pip install -r requirements-tensorflow.txt
    pipenv install --dev
    pipenv install -r requirements.txt
    pipenv run pip install -r requirements-torch.txt
else
    rm Pipfile Pipfile.lock
    pipenv --python 3.8
    pipenv run pip install nvidia-pyindex
    pipenv run pip install -r requirements-tensorflow.txt
    pipenv install --dev -r requirements-dev.txt
    pipenv install -r requirements-tasklib.txt
    pipenv install -r requirements.txt

    whlfile=`ls -1 mia_tasklib-*-py3-none-any.whl`
    pipenv uninstall mia_tasklib
    pipenv install ${whlfile}

    pipenv run pip install -r requirements-torch.txt
fi
