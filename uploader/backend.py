#!/usr/bin/env python3

from pathlib import Path
from typing import Any, Dict, List, Tuple
import logging
import os

from requests_toolbelt import MultipartEncoder, MultipartEncoderMonitor
import requests

BACKEND_URL = os.getenv("MIA_BACKEND_URL", "http://localhost:3000")


def create_upload_callback(file_path: Path):
    filename = str(file_path)

    def callback(monitor: MultipartEncoderMonitor):
        logging.debug("Uploading file '%s' ... %d bytes!", filename,
                      monitor.bytes_read)

    return callback


def is_backend_online() -> bool:
    try:
        res = requests.get(f"{BACKEND_URL}/settings", timeout=10)
        return res.status_code == 200
    except:
        return False


def set_use_batch_algorithms(use_batch: bool) -> bool:
    res = requests.put(f"{BACKEND_URL}/settings",
                       json={"use_batch": use_batch})
    return res.status_code == 200


def get_accept_types() -> Tuple[List[str], Dict[str, Any]]:
    res = requests.get(f"{BACKEND_URL}/accept-types")
    if res.status_code == 200:
        accept_strs = []
        accept_mapping = {}
        accept_types = res.json()
        for accept_type in accept_types:
            if accept_type["enabled"]:
                key = f".{accept_type['suffix']}"
                accept_mapping[key] = accept_type
                accept_strs.append(key)
        return accept_strs, accept_mapping
    return [], {}


def upload_media_file(file_path: Path, media_type: str,
                      relative_path: Path) -> bool:
    e = MultipartEncoder(
        fields={
            "media_type": media_type,
            "path": str(relative_path),
            "media_file": (file_path.name, open(file_path, "rb"))
        })

    logging.debug("Uploading file '%s' ... 0 bytes!", str(file_path))
    cb = create_upload_callback(file_path)
    m = MultipartEncoderMonitor(e, cb)
    res = requests.post(f"{BACKEND_URL}/media-files",
                        data=m,
                        headers={"Content-Type": m.content_type})
    return res.status_code == 201


def upload_batch_job(file_path: Path, media_type: str,
                     relative_path: Path) -> bool:
    files = {"media_file": open(file_path, "rb")}
    data = {"media_type": media_type, "path": str(relative_path)}
    res = requests.post(f"{BACKEND_URL}/jobs/media-files",
                        files=files,
                        data=data)
    return res.status_code == 201
