#!/usr/bin/env python3

from multiprocessing import Process
from multiprocessing.managers import SyncManager
from pathlib import Path
from typing import List, Tuple
import argparse
import logging
import time

from watchfiles import Change, watch

from backend import (set_use_batch_algorithms, get_accept_types,
                     upload_media_file, upload_batch_job)


class AcceptTypesFilter:
    accept_types: Tuple[str, ...] = ()

    def __init__(self, accept_types: List[str]):
        self.accept_types = tuple(accept_types)

    def __call__(self, change: Change, path: str) -> bool:
        return (change == Change.added and path.endswith(self.accept_types)
                and Path(path).is_file())


class Uploader:
    tasks = []

    def __init__(self):
        self._manager = SyncManager()
        self._manager.start()
        self.stop_event = self._manager.Event()
        self.upload_files = self._manager.Queue()

    def watch_for_files(self, dir_path: Path):
        logging.debug("'watch_for_files' has started!")
        accept_types, accept_mapping = get_accept_types()
        for changes in watch(dir_path,
                             watch_filter=AcceptTypesFilter(accept_types),
                             stop_event=self.stop_event):
            for _, filepath in changes:
                file_path = Path(filepath)
                if file_path.suffix in accept_mapping:
                    self.upload_files.put([
                        file_path,
                        file_path.stat().st_size,
                        accept_mapping[file_path.suffix]["media_type"]
                    ])
        self.upload_files.put(None)
        logging.debug("'watch_for_files' has ended!")

    def upload_to_backend(self, dir_path: Path, create_batch_job: bool):
        logging.debug("'upload_to_backend' has started!")
        while True:
            upload_file = self.upload_files.get()
            if upload_file is None:
                break
            file_path, file_size, media_type = upload_file
            while True:
                if file_path.stat().st_size == file_size:
                    success = False
                    if create_batch_job:
                        success = upload_batch_job(
                            file_path, media_type,
                            file_path.relative_to(dir_path))
                    else:
                        success = upload_media_file(
                            file_path, media_type,
                            file_path.relative_to(dir_path))
                    logging.info("Uploading file '%s'(%d) ... %s!",
                                 str(file_path),
                                 file_path.stat().st_size,
                                 "success" if success else "failed")
                    break
                time.sleep(1)
                file_size = file_path.stat().st_size
        logging.debug("'upload_to_backend' has ended!")

    def run_tasks(self, dir_path: Path, *, create_batch_job: bool = False):
        self.tasks = [
            Process(target=self.watch_for_files, args=(dir_path, )),
            Process(target=self.upload_to_backend,
                    args=(
                        dir_path,
                        create_batch_job,
                    ))
        ]
        for task in self.tasks:
            task.start()

    def join_tasks(self):
        for task in self.tasks:
            task.join()

    def stop_tasks(self):
        self.stop_event.set()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        fromfile_prefix_chars="@",
        description="""Watch directory for media files and
        upload them to the backend!""")

    use_batch = parser.add_mutually_exclusive_group()
    use_batch.add_argument("--set-use-batch-algorithms",
                           action="store_true",
                           help="run algorithms on batch target")
    use_batch.add_argument("--unset-use-batch-algorithms",
                           action="store_true",
                           help="run algorithms on interactive target")
    batch_job = parser.add_mutually_exclusive_group()
    batch_job.add_argument("--no-create-batch-job",
                           dest="create-batch-job",
                           action="store_false",
                           help="upload media file only (default)")
    batch_job.add_argument("--create-batch-job",
                           action="store_true",
                           help="upload media file and create batch job")

    parser.add_argument("--watch-dir",
                        type=lambda p: Path(p).absolute(),
                        default="./tests",
                        help="directory to watch for (default:./tests)")
    parser.add_argument("--log-level",
                        type=int,
                        choices=[0, 10, 20, 30, 40, 50],
                        default=20,
                        help="logging level (default:20)")
    parser.set_defaults(create_batch_job=False)
    args = parser.parse_args()

    logging.basicConfig(filename="./uploader.log", level=args.log_level)
    logging.getLogger().addHandler(logging.StreamHandler())
    logging.debug("Program has started!")
    if args.set_use_batch_algorithms:
        set_use_batch_algorithms(True)
    if args.unset_use_batch_algorithms:
        set_use_batch_algorithms(False)
    if not args.watch_dir.exists():
        logging.warning("Directory '%s' is non-existent!", str(args.watch_dir))
    else:
        uploader = Uploader()
        uploader.run_tasks(args.watch_dir,
                           create_batch_job=args.create_batch_job)
        while True:
            try:
                input("Press 'Ctrl-D' to quit the program...\n")
            except:
                uploader.stop_tasks()
                break
        uploader.join_tasks()
    logging.debug("Program has ended!")
